<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 text-center">
				<h2 class="section-heading">Позвоните нам!</h2>
				<hr class="primary">
				<p>Выбрали какой-то вариант или есть вопросы? Позвоните по номеру ниже или отправьте сообщение по почте и мы сделаем все, что возможно в наших силах!</p>
			</div>
			<div class="col-lg-4 col-lg-offset-2 text-center">
				<i class="fa fa-phone fa-3x sr-contact"></i>
				<p>999-999-99999</p>
			</div>
			<div class="col-lg-4 text-center">
				<i class="fa fa-envelope-o fa-3x sr-contact"></i>
				<p>
					<a href="mailto:your-email@your-domain.com">feedback@air-gear.com</a>
				</p>
			</div>
		</div>
	</div>
</section>

<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="<c:url value="/resources/vendor/scrollreveal/scrollreveal.min.js" />"></script>
<script src="<c:url value="/resources/vendor/magnific-popup/jquery.magnific-popup.min.js" />"></script>

<!-- Theme JavaScript -->
<script src="<c:url value="/resources/js/creative.min.js" />"></script>

<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

</body>

</html>

