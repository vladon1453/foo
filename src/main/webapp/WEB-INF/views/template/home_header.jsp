<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>

<title>Air Gear</title>

<!-- jQuery -->
<script src="<c:url value="/resources/vendor/jquery/jquery.min.js" />"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js" />"></script>

<!--Angular JS-->

<script src="<c:url value="/resources/angularjs/angular.min.js" />"></script>

<!-- Bootstrap Core CSS -->
<link href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css" />" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<c:url value="/resources/vendor/font-awesome/css/font-awesome.min.css" />" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

<!-- Plugin CSS -->
<link href="<c:url value="/resources/vendor/magnific-popup/magnific-popup.css" />" rel="stylesheet">

<!-- Theme CSS -->
<link href="<c:url value="/resources/css/creative.min.css" />" rel="stylesheet">

<!-- Table CSS -->
<link href="<c:url value="/resources/css/tableStyle.css" />" rel="stylesheet">

</head>

<body id="page-top">
	<c:url var="logoutUrl" value="/logout" />
	<form action="${logoutUrl}" method="post" id="LogoutForm">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
	
	<!-- Navigation -->
	<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">

			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
				</button>
				<a class="navbar-brand page-scroll" href="#page-top">Air Gear</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="page-scroll" href="#about">О магазине</a></li>
					<li><a class="page-scroll" href="#services">Ассортимент</a></li>
					<li><a class="page-scroll" href="#portfolio">Портфолио</a></li>
					<li><a class="page-scroll" href="#contact">Контакты</a></li>

					<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
					<c:if test="${pageContext.request.userPrincipal.name !=null}">
						<li><a>Приветствуем: ${pageContext.request.userPrincipal.name}</a></li>
						<li><a href=# onclick="document.getElementById('LogoutForm').submit()">Выйти</a></li>
						<c:if test="${pageContext.request.userPrincipal.name == 'admin'}">
							<li><a href="<c:url value="/admin" />#portfolio">Админ</a></li>
						</c:if>
					</c:if>

					<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
					<c:if test="${pageContext.request.userPrincipal.name == null}">
						<li><a class="page-scroll" href="<c:url value="/login" />#portfolio">Авторизация</a></li>
					</c:if>

				</ul>
			</div>
		</div>
	</nav>