<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="/WEB-INF/views/template/home_header.jsp"%>
<!-- Masthead -->
<header>
    <div class="header-content">
        <div class="header-content-inner">
            <h1 id="homeHeading">Добро пожаловать в Air Gear -
                интернет-магазин бытовой техники!</h1>
            <hr>
            <a href="#about"
                class="btn btn-primary btn-xl page-scroll sr-button">Что
                у нас есть?</a>
        </div>
    </div>
</header>

<!-- About Section -->
<section class="bg-primary" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h2 class="section-heading">У нас есть то, что вам
                    нужно!</h2>
                <hr class="light">
                <p class="text-faded">Холодильники, кухонные плиты,
                    духовые шкафы, посудомоечные машины, кофемашины -
                    это только самая малая часть нашего ассортимента!</p>
                <a class="page-scroll btn btn-default btn-xl sr-button"
                    href="#services">Ассортимент</a>
            </div>
        </div>
    </div>
</section>

<!-- Services Section -->
<section id="services">
    <div class="container-fluid ">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Ассортимент</h2>
                <hr class="primary">
            </div>
        </div>
    </div>
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>Фото</th>
                <th>Модель</th>
                <th>Категория</th>
                <th>Состояние</th>
                <th>Цена</th>
                <th>Превью</th>
            </tr>
        </thead>
        <c:forEach items="${products}" var="product">
            <tr>
                <td><img src="#" alt="image" /></td>
                <td>${product.productName}</td>
                <td>${product.productCategory}</td>
                <td>${product.productCondition}</td>
                <td>${product.productPrice}BLR</td>
                <td><a
                    href="<c:url value="/viewProduct/${product.productId}"/>">
                        <span class="glyphicon glyphicon-info-sign"></span>
                </a></td>
            </tr>
        </c:forEach>
    </table>
</section>

<!-- Portfolio Section -->

<section id="portfolio">
    <div class="container-fluid p-0">
        <div class="row no-gutter popup-gallery">
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box"
                    href="<c:url value="/resources/img/portfolio/fullsize/1.jpg" />">
                    <img class="img-fluid"
                    src="<c:url value="/resources/img/portfolio/thumbnails/1.jpg" />"
                    alt="">
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">
                            Категория</div>
                        <div class="project-name">Холодильники</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box"
                    href="<c:url value="/resources/img/portfolio/fullsize/2.jpg" />">
                    <img class="img-fluid"
                    src="<c:url value="/resources/img/portfolio/thumbnails/2.jpg" />"
                    alt="">
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">
                            Категория</div>
                        <div class="project-name">Кухонные плиты</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box"
                    href="<c:url value="/resources/img/portfolio/fullsize/3.jpg" />">
                    <img class="img-fluid"
                    src="<c:url value="/resources/img/portfolio/thumbnails/3.jpg" />"
                    alt="">
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">
                            Категория</div>
                        <div class="project-name">Посудомойки</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box"
                    href="<c:url value="/resources/img/portfolio/fullsize/4.jpg" />">
                    <img class="img-fluid"
                    src="<c:url value="/resources/img/portfolio/thumbnails/4.jpg" />"
                    alt="">
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">
                            Категория</div>
                        <div class="project-name">Духовки</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box"
                    href="<c:url value="/resources/img/portfolio/fullsize/5.jpg" />">
                    <img class="img-fluid"
                    src="<c:url value="/resources/img/portfolio/thumbnails/5.jpg" />"
                    alt="">
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">
                            Категория</div>
                        <div class="project-name">Морозильные камеры</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box"
                    href="<c:url value="/resources/img/portfolio/fullsize/6.jpg" />">
                    <img class="img-fluid"
                    src="<c:url value="/resources/img/portfolio/thumbnails/6.jpg" />"
                    alt="">
                    <div class="portfolio-box-caption p-3">
                        <div class="project-category text-white-50">
                            Категория</div>
                        <div class="project-name">Кофеварки</div>
                    </div>
                </a>
            </div>
        </div>
    </div>    
</section>
<%@include file="/WEB-INF/views/template/footer.jsp"%>
