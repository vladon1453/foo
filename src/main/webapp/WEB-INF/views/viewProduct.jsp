<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<!-- Services Section -->
<%@include file="/WEB-INF/views/template/header.jsp"%>

<section id="services">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Превью товара</h2>
				<hr class="primary">
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<img src="#" alt="image" style="width: 100%; height: 300px;" />
			</div>
			<div class="col-md-5">
				<h3>Модель: ${product.productName}</h3>
				<p>Описание: ${product.productDescription}</p>
				<p>Производитель: ${product.productManufacturer}</p>
				<p>Категория: ${product.productCategory}</p>
				<p>Цена: ${product.productPrice} BLR</p>
			</div>
		</div>
	</div>
</section>
<%@include file="/WEB-INF/views/template/footer.jsp"%>


