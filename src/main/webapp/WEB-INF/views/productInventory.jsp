<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@include file="/WEB-INF/views/template/header.jsp"%>

<section id="services">
	<div class="container-fluid ">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Каталог товаров</h2>
				<hr class="primary">
				<div class="container"></div>
			</div>
		</div>

		<form:form method="get" action="${pageContext.request.contextPath}/admin/productInventory/search"
			modelAttribute="searchCriteria">

			<div class="container col-md-6 col-md-offset-3">
				<div class="input-group">
					<form:errors path="searchString" cssStyle="color: red;" />
					<form:input id="searchString" path="searchString" placeholder="Search products..." cssClass="form-control" />
					<span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
				</div>
				<br>
			</div>
		</form:form>
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th>Фото</th>
					<th>Модель</th>
					<th>Категория</th>
					<th>Состояние</th>
					<th>Цена</th>
					<th>Превью</th>
				</tr>
			</thead>
			<c:forEach items="${products}" var="product">
				<tr>
					<td><img src="#" alt="image" /></td>
					<td>${product.productName}</td>
					<td>${product.productCategory}</td>
					<td>${product.productCondition}</td>
					<td>${product.productPrice}BLR</td>
					<td><a href="<c:url value="/viewProduct/${product.productId}"/>"> <span
							class="glyphicon glyphicon-info-sign"></span>
					</a></td>
				</tr>
			</c:forEach>
		</table>
		<h3>
			<a href="<c:url value="/admin/productInventory/addProduct"/>" class="btn btn-lg btn-success"> Добавить товар </a>
		</h3>
	</div>
</section>

<%@include file="/WEB-INF/views/template/footer.jsp"%>