<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@include file="/WEB-INF/views/template/header.jsp"%>

<section id="portfolio">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Страница авторизации</h2>
				<p>Привет! Это страница авторизации.</p>
				<hr class="primary">
				<br>
			</div>
			
			<div class="container col-md-6 col-md-offset-3">

				<c:if test="${not empty error}">
					<div class="error">${error}</div>
				</c:if>
				<c:if test="${not empty msg}">
					<div class="msg">${msg}</div>
				</c:if>

				<form name='loginForm' action="<c:url value='/j_spring_security_check' />" method='POST'>
					<div class="form-group">
						<label for="username">Имя пользователя: </label> <input type="text" id="username" name="username"
							class="form-control" />
					</div>
					<div class="form-group">
						<label for="password">Пароль: </label> <input type="password" id="password" name="password" class="form-control" />
					</div>

					<input type="submit" value="Подтвердить" class="btn btn-success" /> <input type="hidden"
						name="${_csrf.parameterName}" value="${_csrf.token}" />
				</form>
			</div>
		</div>
	</div>
</section>

<%@include file="/WEB-INF/views/template/footer.jsp"%>