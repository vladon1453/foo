<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@include file="/WEB-INF/views/template/header.jsp"%>

<section id="portfolio">
	<div class="container-fluid">
		<div class="col-lg-8 col-lg-offset-2 text-center">
			<h1 class="section-heading">
				<c:if test="${pageContext.request.userPrincipal.name !=null}">
					<h2>
						Приветствуем: <span class="text-uppercase">${pageContext.request.userPrincipal.name} </span>
					</h2>
				</c:if>
			</h1>
			<p>Привет! Это страница администратора.</p>
			<hr class="primary">
			<br>
			<h3>
				<a href="<c:url value="/admin/productInventory"/>" class="btn btn-lg btn-success"> Каталог товаров </a>
			</h3>
			<p>Cдесь ты можешь просматривать,редактировать и добовлять новый ассортимент!</p>
		</div>
	</div>
</section>

<%@include file="/WEB-INF/views/template/footer.jsp"%>