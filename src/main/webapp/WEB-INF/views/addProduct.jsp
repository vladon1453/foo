<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@include file="/WEB-INF/views/template/header.jsp"%>
<section id="services">
    <div class="container-fluid">
        <div class="col-lg-8 col-lg-offset-2 text-center">
            <h1 class="section-heading">Страница добавления продукта</h1>
            <hr class="primary">
        </div>
    </div>
    <div class="container col-md-6 col-md-offset-3">
        <form:form action="${pageContext.request.contextPath}/admin/product/addProduct" method="post"
                   modelAttribute="product" enctype="multipart/form-data">

            <div class="form-group">
                <label for="name">Название</label><form:errors path="productName" cssStyle="color: red;" />
                <form:input path="productName" id="name" cssClass="form-control" />
            </div>
            
            <div class="form-group">
                <label for="category">Категория</label>
                <label class="checkbox-inline">
                    <form:radiobutton path="productCategory" id="category" value="Холодильники" />Холодильники
                </label>
                <label class="checkbox-inline">
                    <form:radiobutton path="productCategory" id="category" value="Духовые шкафы" />Духовые шкафы
                </label>
                <label class="checkbox-inline">
                    <form:radiobutton path="productCategory" id="category" value="Стиральные машины" />Стиральные машины
                </label>
                <label class="checkbox-inline">
                    <form:radiobutton path="productCategory" id="category" value="Посудомойки" />Посудомойки
                </label>
                <label class="checkbox-inline">
                    <form:radiobutton path="productCategory" id="category" value="Кофеварки" />Кофеварки
                </label>
            </div>

            <div class="form-group">
                <label for="description">Описание</label>
                <form:textarea path="productDescription" id="description" cssClass="form-control" />
            </div>

            <div class="form-group">
                <label for="price">Цена</label><form:errors path="productPrice" cssStyle="color: red;" />
                <form:input path="productPrice" id="price" cssClass="form-control" />
            </div>

            <div class="form-group">
                <label for="condition">Состояние</label>
                <label class="checkbox-inline">
                    <form:radiobutton path="productCondition" id="condition" value="Новое" />Новое
                </label>
                <label class="checkbox-inline">
                    <form:radiobutton path="productCondition" id="condition" value="БУ" />БУ
                </label>
            </div>

            <div class="form-group">
                <label for="status">Статус</label>
                <label class="checkbox-inline">
                    <form:radiobutton path="productStatus" id="status" value="active" />Активно
                </label>
                <label class="checkbox-inline">
                    <form:radiobutton path="productStatus" id="status" value="inactive" />Неактивно
                </label>
            </div>

            <div class="form-group">
                <label for="unitInStock">Количество</label><form:errors path="unitInStock" cssStyle="color: red;" />
                <form:input path="unitInStock" id="unitInStock" cssClass="form-control" />
            </div>

            <div class="form-group">
                <label for="manufacturer">Производитель</label>
                <form:input path="productManufacturer" id="manufacturer" cssClass="form-control" />
            </div>
            <br>
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <input type="submit" value="submit" class="btn btn-success" />
                <a href="<c:url value="/admin/productInventory" />" class="btn btn-primary">Назад</a>
            </div>
        </form:form>
    </div>

</section>
<div style="padding-bottom: 5%" />
<div class="clearfix"/></div>