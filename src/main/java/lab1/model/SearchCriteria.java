package lab1.model;

public class SearchCriteria{

    /**
     * The user-provided search criteria for finding Products.
     */
    private String searchString;

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }
}
