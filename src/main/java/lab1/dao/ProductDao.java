package lab1.dao;

import java.util.List;


import lab1.model.Product;
import lab1.model.SearchCriteria;

public interface ProductDao {
    void addProduct(Product product);
    Product getProductById(int id);
    List<Product> getAllProducts();
    void deleteProduct(int id);
    List<Product> findProducts(SearchCriteria criteria);
}

