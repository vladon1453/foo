package lab1.dao.impl;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import lab1.dao.ProductDao;
import lab1.model.Product;
import lab1.model.SearchCriteria;

@Repository
@Transactional
public class ProductDaoImpl implements ProductDao {

    @Autowired
    private SessionFactory sessionfactory;

    public void addProduct(Product product) {
        Session session = sessionfactory.getCurrentSession();
        session.saveOrUpdate(product);
        session.flush();
    }

    public Product getProductById(int id) {
        Session session = sessionfactory.getCurrentSession();
        Product product = (Product) session.get(Product.class, id);
        return product;
    }

    public List<Product> getAllProducts() {
        Session session = sessionfactory.getCurrentSession();
        Query query = session.createQuery("FROM Product");
        List<Product> products = query.list();
        session.flush();
        return products;
    }

    public void deleteProduct(int id) {
        Session session = sessionfactory.getCurrentSession();
        session.delete(getProductById(id));
        session.flush();
    }

    public List<Product> findProducts(SearchCriteria criteria) {
        String pattern = getSearchPattern(criteria);
        Session session = sessionfactory.getCurrentSession();
        Query query = session.createQuery("select p from Product p where lower(p.productName) like " + pattern
                + " or lower(p.productCategory) like " + pattern + " or lower(p.productDescription) like " + pattern
                + " or lower(p.productManufacturer) like " + pattern);
        List<Product> productList = query.list();
        session.flush();
        return productList;
    }

    private String getSearchPattern(SearchCriteria criteria) {
        if (StringUtils.hasText(criteria.getSearchString())) {
            return "'%" + criteria.getSearchString().toLowerCase().replace('*', '%') + "%'";
        } else {
            return "'%'";
        }
    }

}
