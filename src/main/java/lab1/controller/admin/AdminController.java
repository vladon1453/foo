package lab1.controller.admin;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import lab1.dao.ProductDao;
import lab1.model.Product;
import lab1.model.SearchCriteria;

public class AdminController {
    @Autowired
    private ProductDao productDao;

    @RequestMapping("/admin")
    public String adminPage() {
        return "admin";
    }

    @RequestMapping("/admin/productInventory")
    public String productInventory(SearchCriteria searchCriteria, Model model) {
        List<Product> productList2 = productDao.getAllProducts();
        model.addAttribute("products", productList2);
        return "productInventory";
    }

    @RequestMapping("/admin/productInventory/addProduct")
    public String addProduct(Model model) {
        Product product = new Product();
        product.setProductCategory("Холодильники");
        product.setProductCondition("Новое");
        product.setProductStatus("Есть");
        model.addAttribute("product", product);
        return "addProduct";
    }

    @RequestMapping(value = "/admin/productInventory/addProduct", method = RequestMethod.POST)
    public String addProductPost(@ModelAttribute("product") Product product) {
        productDao.addProduct(product);
        return "redirect:/admin/productInventory";
    }

    @RequestMapping(value = "/admin/productInventory/search", method = RequestMethod.GET)
    public String list(SearchCriteria criteria, Model model) {
        List<Product> productList3 = productDao.findProducts(criteria);
        model.addAttribute("products", productList3);
        return "productInventory";
    }
}
