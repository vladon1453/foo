package lab1.controller;

import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import lab1.dao.ProductDao;
import lab1.model.Product;
import lab1.model.SearchCriteria;

@Controller
public class HomeController {
    @Autowired
    private ProductDao productDao;

    @RequestMapping("/")
    public String home(Model model) {
        List<Product> productList1 = productDao.getAllProducts();
        model.addAttribute("products", productList1);
        return "home";
    }

    @RequestMapping("/viewProduct/{productId}")
    public String viewProduct(@PathVariable int productId, Model model) throws IOException {
        Product product = productDao.getProductById(productId);
        model.addAttribute("product", product);
        return "viewProduct";
    }

    @RequestMapping("/login")
    public String login(@RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout, Model model) {
        if (error != null) {
            model.addAttribute("error", "Неверное имя пользователя или пароль!");
        }
        if (logout != null) {
            model.addAttribute("msg", "Вы успешно разлогинились!");
        }
        return "login";
    }

}
